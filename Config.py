'''
Description: 项目的配置文件
Version: 1.0.0
Author: bill wang
Date: 2023-05-20 14:54:53
LastEditors: Please set LastEditors
LastEditTime: 2023-07-25 17:23:32
'''
import os

#得到项目的绝对的路径
baseDir = os.path.abspath(os.path.dirname(__file__))
# 日志级别
LOGGING_CRITICAL = 50
LOGGING_ERROR = 40
LOGGING_WARNING = 30
LOGGING_INFO = 20
LOGGING_DEBUG = 10


class Config:
    '''
    公共的配置类。
    各环境共用的配置都维护在这个类中。
    '''
    SECRET_KEY = os.environ.get("SECRET_KEY") if os.environ.get(
        "SECRET_KEY") is not None else "Abcd1234"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 标识是否需要redis支持
    IS_REDIS_SUPPORT = True

    @staticmethod
    def init_app(app):
        '''
        app 是传入的flask application 的一个实例
        本方法是用于各个环境下初始化程序的入口
        '''
        pass


class DevelopmentConfig(Config):
    '''
    开发环境的配置类。
    '''
    DEBUG = True
    # 主数据库连接字符串
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:123456@192.168.220.210:31101/horaeflow"
    SQLALCHEMY_BINDS = {
        'ops': "mysql+pymysql://ops:ops_test@10.131.7.18:3306/ops"  # ops数据库
    }
    SQLALCHEMY_RECORD_QUERIES = False  #表示开启查询记录功能
    SQLALCHEMY_TRACK_MODIFICATIONS = False  #表示启用 SQLAlchemy 的修改跟踪功能
    SQLALCHEMY_ECHO = True  # 是否打开sqlalchemy.engine，sqlalchemy.pool 包日志
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False  # 是否打开请求结束后自动提交数据库事务，如果False时，则要手动commit提交
    SQLALCHEMY_ENGINE_OPTIONS = {
        'pool_size': 10,  # 连接池大小
        'max_overflow': 20,  # 临时许可最大值
        'pool_timeout': 10,  # 获取连接的超时时间
        'pool_recycle': 3600,  # 连接最大回收时间 1小时
        'execution_options': {
            'isolation_level': 'READ COMMITTED'  #数据库事务级别
        }
    }
    SQLALCHEMY_DATABASE_MODEL_RESET = False
    #REDIS_SENTINEL_HOSTS = [('192.168.210.112', 26379),
    #                        ('192.168.210.207', 26379),
    #                        ('192.168.210.139', 26379)]
    #REDIS_SENTINEL_SERVICE_NAME = "mymaster"
    REDIS_DB = 31
    REDIS_PASSWORD = '0123456'
    REDIS_HOST = "192.168.210.112"
    REDIS_PORT = 6379
    LOGGING_LEVEL = LOGGING_DEBUG
    LOGGING_PATH = r'E:\log'
    JENKINS_URL = 'http://192.168.113.32:8080'
    JENKINS_USERNAME = 'root'
    JENKINS_PASS_OR_TOKEN = '11c423e1b9e71bd78901f906d0f152481f'
    #CAS_SERVER_URL = "http://192.168.210.229/scas-server"
    #CAS_VALIDATE_URL = "http://192.168.210.229/scas-server"
    #CAS_AFTER_LOGIN_URL = "http://127.0.0.1:5000/"
    #CAS_TOKEN_SESSION_KEY = '_CAS_TOKEN'
    #CAS_USERNAME_SESSION_KEY = 'CAS_USERNAME'


class TestingConfig(Config):
    '''
    测试环境的配置类。
    '''
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "测试库地址"


class ProductionConfig(Config):
    '''
    生产环境的配置类
    '''
    SQLALCHEMY_DATABASE_URI = "生产库地址"


#所有配置的一个字典
config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig
}
