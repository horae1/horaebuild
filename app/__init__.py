'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-05-20 15:26:35
LastEditors: Please set LastEditors
LastEditTime: 2023-07-25 17:36:02
'''
'''
app包的初始化入口
'''
import json
import logging
import os
from functools import wraps
from typing import Optional

import jenkins
from Config import config
from flask import Flask, abort, current_app, jsonify, request
from flask.logging import default_handler
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from jose import jwt

from .redis_ch import RedisService

logger = logging.getLogger(__name__)


class ChFlaskApp(Flask):
    '''
    继承Flask类,构建一个自定义的ChFlaskApp类,用于可以封装一些自定义的属性。
    1. 加入redisService单例属性,用于加载redis的相关的配置信息
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._redisService = None

    @property
    def redisService(self):
        return self._redisService

    @redisService.setter
    def redisService(self, value):
        assert value is not None, "redis service不能None"
        if self._redisService is None:
            self._redisService = value


def create_app(config_name):
    '''
    flash application的全局对象的一个工厂方法
    '''
    app: ChFlaskApp = ChFlaskApp(__name__)
    currentConfigObject = config[config_name]
    app.config.from_object(currentConfigObject)
    # 读取配置中的debug设置
    app.debug = app.config['DEBUG']
    # 初始化配置对象
    currentConfigObject.init_app(app)

    # 配置应用日志
    config_logging(app)

    # 配置redis
    is_redis_support = app.config.get('IS_REDIS_SUPPORT')
    if is_redis_support:
        config_redis(app)
    # 注册 main Blueprint
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    # 配置restful api
    config_restful(app)

    # create the extension
    database = SQLAlchemy()
    database.init_app(app)

    is_database_model_reset = app.config.get('SQLALCHEMY_DATABASE_MODEL_RESET')
    if is_database_model_reset:
        with app.app_context():
            from .db.models import User
            database.drop_all()
            database.create_all()

    # 配置python-jenkins
    config_jenkins(app)

    # 返回实例化的flash application对象
    return app


def config_logging(app: ChFlaskApp):
    '''
    配置日志级别
    '''

    # 配置日志输出级别
    logging_level = app.config.get("LOGGING_LEVEL")
    if logging_level is None:
        logging_level = 30  # 默认warning级别
    configPath = app.config.get("LOGGING_PATH")
    if configPath is None:
        configPath = "/u03/log/"
    log_file = os.path.join(configPath, 'app.log')
    app.logger.setLevel(logging_level)

    # 创建文件处理器，并设置日志输出格式
    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging_level)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)-5s [%(threadName)-15s] %(name)-50s : %(message)s'
    )
    file_handler.setFormatter(formatter)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging_level)
    console_handler.setFormatter(formatter)

    # 将文件处理器添加到应用程序的日志处理器中
    # 先将默认的日志handler给清理掉,否则，控制台会输出两行日志
    app.logger.removeHandler(default_handler)
    app.logger.addHandler(file_handler)
    app.logger.addHandler(console_handler)


def config_redis(app: ChFlaskApp):
    '''
    配置redis
    '''
    host = app.config.get("REDIS_HOST")
    db = app.config.get("REDIS_DB")
    port = app.config.get("REDIS_PORT")
    password = app.config.get("REDIS_PASSWORD")
    sentinel_hosts = app.config.get('REDIS_SENTINEL_HOSTS')
    redis_service_name = app.config.get('REDIS_SENTINEL_SERVICE_NAME')
    redisService = RedisService(host=host,
                                db=db,
                                port=port,
                                password=password,
                                socket_timeout=5,
                                sentinel_hosts=sentinel_hosts,
                                max_connection=10,
                                is_readonly=False,
                                sentinel_service_name=redis_service_name)
    logger.debug("初始化redisService")
    app.redisService = redisService


def config_restful(app: ChFlaskApp):
    from .api.hello import HelloWorld
    api = Api(
        app,
        prefix='/api',
        decorators=[token_required],
    )

    api.add_resource(HelloWorld,
                     "/v1/hello",
                     resource_class_kwargs={'api': api})


# 装饰器函数
def token_required(f):

    @wraps(f)
    def decorated_function(*args, **kwargs):
        # 判断用户是否已登录，例如通过检查 session 或 token
        """ 
        1. 从当前的请求中取token
        2. 从redis中取当前请求的用户信息
        todo
        3. 将当前的用户信息设置不请求级别的变量
        """
        token = get_token()
        redisKey_suffix = jwt.decode(token,
                                     key='abcdefghijklmnopqrstuvwxyz',
                                     algorithms='HS512').get('login_user_key')

        if redisKey_suffix is None:
            abort(401)

        user_info = get_user_info_from_redis(redisKey_suffix)

        if user_info is None:
            logger.debug("redis中没有取到对应的用户信息,重新登录")
            abort(401)
        return f(*args, **kwargs)

    return decorated_function


def get_token_from_header() -> Optional[str]:
    '''
    从请求头中取token值
    请求头中存储的token的格式为  header 头: Authorization: Bearer XXXX 
    '''
    authorization_header = request.headers.get('Authorization')
    if authorization_header is not None:
        token = authorization_header.split(' ')[1].strip()
        logger.debug("从Authorization Header中取到值为[%s]", token)
        return token


def get_token_from_cookies() -> Optional[str]:
    '''
    从请求的cookie中取token值
    请求头中存储的token的格式为  3.	Cookies: Admin-Token  = XXXX
    '''
    token = request.cookies.get('Admin-Token')
    if token is not None:
        logger.debug(f"从cookie中取到的token为:[{token}]")
        return token


def get_token_from_request() -> Optional[str]:
    '''
    从请求的parameter中取token值,get请求
    requestParam: Authorization = XXXX
    '''
    token = request.args.get('Authorization ')
    if token is not None:
        logger.debug(f"从request parameter中取到的token为:[{token}]")
        return token


def get_token() -> Optional[str]:
    token = get_token_from_header()
    if token is not None:
        return token
    token = get_token_from_cookies()
    if token is not None:
        return token
    token = get_token_from_request()
    if token is not None:
        return token


def get_user_info_from_redis(token: str):
    key = 'login_tokens:'
    key = key + token
    redis_client: RedisService = current_app.redisService  # type: ignore
    redis = redis_client.get_redis_connection()
    value = redis.get(key)
    if value:
        jsonValue = json.loads(value)
        logger.debug("从redis中取出的的值是%s", jsonValue)
        return jsonValue


def config_jenkins(app: ChFlaskApp):
    # 配置日志输出级别
    jinkens_url = app.config.get("JENKINS_URL")
    if jinkens_url is None:
        logger.debug("jenkins url未配置")
        return
    jinkens_username = app.config.get("JENKINS_USERNAME")
    if jinkens_username is None:
        logger.debug("jenkins username未配置")
        return
    jinkens_pass_or_token = app.config.get("JENKINS_PASS_OR_TOKEN")
    if jinkens_pass_or_token is None:
        logger.debug("jenkins 用户密码或token未配置")
        return

    jenkins_server = jenkins.Jenkins(jinkens_url,
                                     username=jinkens_username,
                                     password=jinkens_pass_or_token,
                                     timeout=None)

    app.extensions['jenkins_server'] = jenkins_server


__all__ = ["ChFlaskApp"]
