'''
Description: 
一个restful请求的demo
Version: 1.0.0
Author: bill wang
Date: 2023-07-14 10:01:00
LastEditors: Please set LastEditors
LastEditTime: 2023-07-26 08:36:40
'''
from flask_restful import Api, Resource, reqparse

from .resource import BaseResource


class HelloWorld(BaseResource):

    def __init__(self, api: Api):
        self.api = api

    def get(self):
        return self.success_response(data="HelloWorld!!!")
