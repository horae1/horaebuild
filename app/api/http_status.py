'''
Description: 定义标准的http 状态码枚举
Version: 1.0.0
Author: bill wang
Date: 2023-07-26 09:01:05
LastEditors: Please set LastEditors
LastEditTime: 2023-07-26 13:48:47
'''

from enum import Enum


class HttpStatusCode(Enum):
    OK = 200
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    METHOD_NOT_ALLOWED = 405
    INTERNAL_SERVER_ERROR = 500
    SERVICE_UNAVAILABLE = 503
