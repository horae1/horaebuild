'''
Description: 定义一个标准的分页对象
Version: 1.0.0
Author: bill wang
Date: 2023-07-26 09:01:05
LastEditors: Please set LastEditors
LastEditTime: 2023-07-26 09:08:35
'''


class Pagination:
    '''
    定义一个分页对象
    items:支持分页的数据对象
    page:当前的页码
    per_page:每页的条数
    '''

    def __init__(self, items, page, per_page) -> None:
        self.items = items
        self.page = page
        self.per_page = per_page

    def get_paginated_items(self):
        start = (self.page - 1) * self.per_page
        end = start + self.per_page
        paginated_items = self.items[start:end]
        return paginated_items

    def get_total_pages(self):
        total_pages = len(self.items) // self.per_page
        if len(self.items) % self.per_page != 0:
            total_pages += 1
        return total_pages

    def to_dict(self):
        return {
            "items": self.get_paginated_items(),
            "page": self.page,
            "per_page": self.per_page,
            "total_pages": self.get_total_pages()
        }
