'''
Description: Restful请求的基类,定义标准的返回数据格式
Version: 1.0.0
Author: bill wang
Date: 2023-07-26 08:24:55
LastEditors: Please set LastEditors
LastEditTime: 2023-07-26 13:50:13
'''
from flask_restful import Resource


class BaseResource(Resource):

    def success_response(self,
                         data,
                         message: str = 'Success',
                         code: int = 200):
        return {"code": code, "message": message, "data": data}

    def error_response(self, message: str = "Error", code: int = 500):
        return {"code": code, "message": message, "data": None}
