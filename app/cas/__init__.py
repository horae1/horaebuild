'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-07-04 17:59:40
LastEditors: Please set LastEditors
LastEditTime: 2023-07-05 09:04:44
'''
"""
flask_cas.__init__
"""

from flask import current_app, Flask
from . import cas_routes


class CAS(object):
    """
    Required Configs:
    
    |Key                  |
    |---------------------|
    |CAS_SERVER_URL       | 
    |CAS_VALIDATE_URL     |
    |CAS_AFTER_LOGIN_URL  |

    Optional Configs:

    |Key                      | Default      |
    |-------------------------|--------------|
    |CAS_TOKEN_SESSION_KEY    | _CAS_TOKEN   |
    |CAS_USERNAME_SESSION_KEY | CAS_USERNAME |
    """

    def __init__(self, app=None, url_prefix=None):
        self.app = app
        if app is not None:
            self.init_app(app, url_prefix)

    def init_app(self, app: Flask, url_prefix=None):
        # Configuration defaults
        app.config.setdefault('CAS_TOKEN_SESSION_KEY', '_CAS_TOKEN')
        app.config.setdefault('CAS_USERNAME_SESSION_KEY', 'CAS_USERNAME')

        # Register Blueprint
        app.register_blueprint(cas_routes.cas_blueprint, url_prefix=url_prefix)

        # otherwise fall back to the request context
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

    def teardown(self, exception):
        pass
