'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-07-04 16:27:23
LastEditors: Please set LastEditors
LastEditTime: 2023-07-04 17:50:16
'''
import urllib
from typing import List, Optional, Tuple
from urllib.parse import quote, urlencode, urlparse


def create_url(base: str, path: Optional[str], *query):
    """ Create a url.
    Creates a url by combining base, path, and the query's list of 
    key/value pairs. Escaping is handled automatically. Any 
    key/value pair with a value that is None is ignored.

    Keyword arguments:
    base -- The left most part of the url (ex. http://localhost:5000).
    path -- The path after the base (ex. /foo/bar).
    query -- A list of key value pairs (ex. [('key', 'value')]).
    
    Example usage:
    >>> create_url(
    ...     'http://localhost:5000',
    ...     'foo/bar',
    ...     ('key1', 'value'),
    ...     ('key2', None),     # Will not include None
    ...     ('url', 'http://example.com'),
    ... )
    'http://localhost:5000/foo/bar?key1=value&url=http%3A%2F%2Fexample.com'
    """
    url = base
    # Add the path to the url if its not None.
    if path is not None:
        url = urlparse.urljoin(url, quote(str(path)))
    # Remove key/value pairs with None values.
    if query is None:
        query1 = []
    else:
        # 使用类型断言将 query 转换为 List[Tuple] 类型
        query1 = list(filter(lambda item: item[1] is not None, query))
    # Add the query string to the url
    url = urlparse.urljoin(url, '?{}'.format(urlencode(query)))
    return url


def create_cas_login_url(cas_url, service, *query):
    """ Create a CAS login URL.
    Keyword arguments:
    cas_url -- The url to the default CAS (ex. http://192.168.210.229/scas-server)
    service -- (ex.  http://localhost:5000/login)
    query  -- other query parameters
    Example usage:
    >>> create_cas_login_url(
    ...     'http://192.168.210.229/scas-server',
    ...     'http://localhost:5000',
    ...     ('key1', 'value'),
    ...     ('key2', 'value2),
    ... )
    'http://192.168.210.229/scas-server/login?service=http%3A%2F%2Flocalhost%3A5000'
    """

    # query.insert(0,('service', service))
    if query is None:
        return create_url(cas_url, "/login")
    else:
        return create_url(cas_url, "/login", query)


def create_cas_logout_url(cas_url, url=None):
    """ Create a CAS logout URL.

    Keyword arguments:
    cas_url -- The url to the CAS (ex.  http://192.168.210.229/scas-server)
    url -- (ex.  http://localhost:5000/login)

    Example usage:
    >>> create_cas_logout_url(
    ...     'http:// http://192.168.210.229/scas-server',
    ...     'http://localhost:5000',
    ... )
    'http://192.168.210.229/scas-server/logout?url=http%3A%2F%2Flocalhost%3A5000'
    """
    return create_url(
        cas_url,
        '/logout',
        ('url', url),
    )


def create_cas_validate_url(cas_url, service, ticket):
    """ Create a CAS validate URL.

    Keyword arguments:
    cas_url -- The url to the CAS (ex. http://192.168.210.229/scas-server)
    service -- (ex.  http://localhost:5000/login)
    ticket -- (ex. 'ST-58274-x839euFek492ou832Eena7ee-cas')
 
    Example usage:
    >>> create_cas_validate_url(
    ...     'http://192.168.210.229/scas-server',
    ...     'http://localhost:5000/login',
    ...     'ST-58274-x839euFek492ou832Eena7ee-cas'
    ... )
    'http://http://192.168.210.229/scas-server/serviceValidate?service=http%3A%2F%2Flocalhost%3A5000%2Flogin&ticket=ST-58274-x839euFek492ou832Eena7ee-cas'
    """
    return create_url(
        cas_url,
        '/serviceValidate',
        ('service', service),
        ('ticket', ticket),
    )
