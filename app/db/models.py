'''
Description: 定义db中的模型类
Version: 1.0.0
Author: bill wang
Date: 2023-07-13 09:17:51
LastEditors: Please set LastEditors
LastEditTime: 2023-07-13 18:00:25
'''
from typing import List, Optional

import sqlalchemy as sa
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship

db: SQLAlchemy = current_app.extensions["sqlalchemy"]


class Base(db.Model):  # type: ignore
    __abstract__ = True


# 这是flask_sqlalchemy方式定义数据库模型
class User1(Base):  # type: ignore
    __tablename__ = "temp_user1"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(50), nullable=False)
    fullname = sa.Column(sa.String(100), default="这是默认值")

    # addresses: Mapped[List["Address1"]] = relationship(
    #     back_populates="user", cascade="all, delete-orphan")

    def __repr__(self) -> str:
        return f"User1(id={self.id!r}, name={self.name!r}, fullname={self.fullname!r})"

    pass


class Address1(Base):  # type: ignore
    __tablename__ = "temp_address1"

    id = sa.Column(sa.Integer, primary_key=True)
    email_address = sa.Column(sa.String(150))

    #user_id: Mapped[int] = mapped_column(ForeignKey("user1.id"))

    # user: Mapped["User1"] = db.relationship(back_populates="addresses")

    def __repr__(self) -> str:
        return f"Address(id={self.id!r}, email_address={self.email_address!r})"


class User2(Base):
    __tablename__ = "temp_user2"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(50), nullable=False)
    email = sa.Column(sa.String(120), unique=True)

    posts = db.relationship('Post', backref='post', lazy=True)

    def __repr__(self):
        return f"<User {self.name}>"


class Post(Base):
    __tablename__ = "posts"

    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.String(100), nullable=False)
    content = sa.Column(sa.Text, nullable=False)
    user_id = sa.Column(sa.Integer,
                        sa.ForeignKey('temp_user2.id'),
                        nullable=False)

    def __repr__(self):
        return f"<Post {self.title}>"


class User(Base):
    __tablename__ = "user_account"
    __bind_key__ = 'ops'

    id = mapped_column(sa.BigInteger, primary_key=True)
    name = mapped_column(sa.String(50), nullable=False)
    fullname = mapped_column(sa.String(150))
    nickname = mapped_column(sa.String(30))


# 这是sqlalchemy orm方式定义数据库模型
""" class User(Base):
    __tablename__ = "user_account"
    __bind_key__ = 'ops'
    id: Mapped[sa.Integer] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(sa.String(30))
    fullname: Mapped[Optional[str]]
    addresses: Mapped[List["Address"]] = relationship(
        back_populates="user", cascade="all, delete-orphan")

    def __repr__(self) -> str:
        return f"User(id={self.id!r}, name={self.name!r}, fullname={self.fullname!r})"


class Address(Base):
    __tablename__ = "address"
    __bind_key__ = 'ops'
    id: Mapped[Integer] = mapped_column(primary_key=True)
    email_address: Mapped[str] = mapped_column(String(100))
    user_id: Mapped[int] = mapped_column(ForeignKey("user_account.id"))

    user: Mapped["User"] = relationship(back_populates="addresses")

    def __repr__(self) -> str:
        return f"Address(id={self.id!r}, email_address={self.email_address!r})"
 """
