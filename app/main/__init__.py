'''
main blueprint 包定义
'''

from flask import Blueprint

#构建一个main Blueprint对象
main = Blueprint('main', __name__)

from . import errors, views
