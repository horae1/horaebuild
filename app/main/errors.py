'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-05-20 15:47:58
LastEditors: Please set LastEditors
LastEditTime: 2023-05-20 15:50:57
'''
'''
统一的错误页面路由定义
'''
from flask import render_template

from . import main


@main.app_errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@main.app_errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500
