'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-05-20 15:52:10
LastEditors: Please set LastEditors
LastEditTime: 2023-07-19 16:08:46
'''
'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-05-20 15:52:10
LastEditors: Please set LastEditors
LastEditTime: 2023-07-04 12:38:07
'''
'''
main blueprint的路由定义
'''
import logging

from flask import current_app, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text

from . import main

logger = logging.getLogger(__name__)


@main.route("/", methods=['GET', 'POST'])
def index():
    logger.debug("写入redis一个key")
    #assert not isinstance(current_app, ChFlaskApp),'current app 类型不对'
    redis_client: RedisService = current_app.redisService  # type: ignore
    redis = redis_client.get_redis_connection()
    redis.set("key1", "helloKey")
    keyvalue = redis.get("key1")
    logger.debug("从redis中取出的key1的值是%s", keyvalue)
    # 获取 db 实例
    # db: SQLAlchemy = current_app.extensions["sqlalchemy"]
    # 执行 SQL 查询
    #result = db.session.execute(text("SELECT * FROM sys_menu"))
    # 执行在 "ops" 数据源上的查询,返回当前线程的独立会话对象
    """ with db.session() as session:
        result = session.execute(text("select * from users"),
                                 bind_arguments={'bind': db.engines['ops']
                                                 })  # type: ignore 
    # 获取查询结果
    for row in result:
       logger.debug(row)
    """
    return render_template("index.html")


@main.route('/favicon.ico')
def favicon():
    # 返回一个空响应或者一个自定义的图标文件
    return ''

@main.route('/hellomhx')
def hello():
    # 返回一个空响应或者一个自定义的图标文件
    return 'hello mhx'