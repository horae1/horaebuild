'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-07-03 16:06:01
LastEditors: Please set LastEditors
LastEditTime: 2023-07-04 14:53:37
'''
import socket
from typing import Optional

import redis
from redis.connection import ConnectionPool
from redis.sentinel import Sentinel


class RedisService:

    def __init__(self,
                 host: Optional[str],
                 port: Optional[int],
                 db: Optional[int],
                 password: Optional[str],
                 socket_timeout: Optional[int],
                 sentinel_hosts: Optional[list],
                 max_connection: Optional[int],
                 is_readonly: Optional[bool],
                 sentinel_service_name: str = "mymaster"):
        '''
          传入构建redisService的相关参数。目前只支持以下两种redis部署方式
          1.redis单机部署
          2.redis sentinel集群部署
          redis cluster 部署暂未支持
          注意：
          1.当sentinel_hosts和sentinel_service_name参数不空时,启用sentinel连接。
          2.当connection_pool不为空时，如果是直接连接，则使用该连接池进行连接。
          参数说明
          host:redis的主机名或ip地址
          port:redis的端口地址
          db  :redis连接的db index
          password:连接redis的password值
          socket_timeout连接超时设置 单位为S
          connection_pool:redis连接池对象
          sentinel_hosts : sentinel服务器列表
          sentinel_service_name:sentinel serviceName.
        '''
        self.host = host
        self.port = port
        self.db = db
        self.password = password
        self.socket_timeout = socket_timeout
        self.sentinel_hosts = sentinel_hosts
        self.sentinel_service_name = sentinel_service_name
        self.max_connection = max_connection
        self.is_readonly = is_readonly

    def get_redis_connection(self):
        '''
        得到一个redis的连接对象。
        '''
        if self.db is None or self.db < 0:
            redis_db = 0
        else:
            redis_db = self.db

        if self.port is None:
            redis_port = 6379
        else:
            redis_port = self.port

        if self.max_connection is None:
            max_conn = 10
        else:
            max_conn = self.max_connection

        if self.is_readonly is None:
            is_readonly = False
        else:
            is_readonly = self.is_readonly
        # sentinel连接方式,默认返回

        socket_keepalive_options = {
            socket.TCP_KEEPIDLE: 60,
            socket.TCP_KEEPINTVL: 10,
            socket.TCP_KEEPCNT: 5
        }

        if self.sentinel_hosts and self.sentinel_service_name:
            sentinel = Sentinel(self.sentinel_hosts)
            if is_readonly:
                return sentinel.slave_for(
                    self.sentinel_service_name,
                    socket_timeout=self.socket_timeout,
                    socket_connect_timeout=self.socket_timeout,
                    socket_keepalive=True,
                    retry_on_timeout=True,
                    socket_keepalive_options=socket_keepalive_options,
                    db=self.db,
                    decode_responses=True,
                    password=self.password)
            else:
                return sentinel.master_for(
                    self.sentinel_service_name,
                    socket_timeout=self.socket_timeout,
                    socket_connect_timeout=self.socket_timeout,
                    socket_keepalive=True,
                    retry_on_timeout=True,
                    socket_keepalive_options=socket_keepalive_options,
                    db=self.db,
                    decode_responses=True,
                    password=self.password)

        else:
            # 直接连接
            # 如果是直接连接，要手动关闭redis的连接
            assert self.host is not None, "redis的host不能为空"
            conn_pool = ConnectionPool(
                max_connections=max_conn,
                host=self.host,
                port=redis_port,
                db=redis_db,
                socket_timeout=self.socket_timeout,
                socket_connect_timeout=self.socket_timeout,
                socket_keepalive=True,
                retry_on_timeout=True,
                socket_keepalive_options=socket_keepalive_options,
                decode_responses=True,
                password=self.password)
            return redis.Redis(connection_pool=conn_pool)

    def execute_redis_command(self, command, *args, **kwargs):
        '''
        执行一个redis的命令。
        '''
        redis_connection = self.get_redis_connection()
        return redis_connection.execute_command(command, *args, **kwargs)
