'''
Description: 
Version: 1.0.0
Author: bill wang
Date: 2023-05-20 15:58:32
LastEditors: Please set LastEditors
LastEditTime: 2023-07-05 12:56:40
'''
import os

from app import create_app
from Config import config

app = create_app("development")
app.run(host="0.0.0.0", port=5000, debug=True)
